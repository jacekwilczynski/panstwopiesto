<?php
require_once 'sendPrivacyEmail.php';

// Register AJAX route
add_action('rest_api_init', function () {
    register_rest_route('form-confirm/v1', '/send', array(
        'methods'  => 'POST',
        'callback' => function ($request) {
            $email  = filter_var($request->get_param('email'), FILTER_SANITIZE_EMAIL);
            $result = sendPrivacyEmail($email);

            return array('sent' => $result);
        },
    ));
});

// Hook into Woocommerce
add_action('woocommerce_after_checkout_validation', function ($formData) {
    $email = filter_var($formData['billing_email'], FILTER_SANITIZE_EMAIL);
    sendPrivacyEmail($email);
});
