<?php

function sendPrivacyEmail($email)
{
    $subject = 'Informacja o przetwarzaniu danych osobowych';

    $headers = "From: " . 'PaństwoPiesto.pl' . ' <biuro@panstwopiesto.pl>' . "\r\n";
    $headers .= "Reply-To: " . '<biuro@panstwopiesto.pl>' . "\r\n";
    $headers .= "CC: " . $email . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    $message = file_get_contents(__DIR__ . '/privacy-email-template.html');

    $result = wp_mail($email, $subject, $message, $headers);

    return array('sent' => $result);
}
